sum100_2 = 0
num = 0  # 不从1开始的原因是最后的和从1开始加，不能略过1
while num < 100:  # < 100: 为了避免多加101这个数
    num += 1  # 先+1为了避免因为continue出现死循环
    if num == 2:
        continue  # 跳过本次循环。执行下次的循环
        print("continue后的代码都不会执行")
    sum100_2 += num
print("sum100:", sum100_2)
print("===========================================")
sum100 = 0
counter = 1
while counter <= 100:
    if counter == 500:
        break  # 终止while循环，执行循环外部的逻辑
        print("break后面的代码都不会执行")
    sum100 += counter  # sum100 = sum100 + counter
    counter += 1 
else:
    print("循环碰到break，else不会执行")
print("sum100:", sum100)