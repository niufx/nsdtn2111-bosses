# if 特殊判断
if -0.0:  # if bool(-0.0) -> if False
    print("-0.0 YES")
else:
    print("-0.0 NO")
print("==============================")
if "":  # if bool("") -> if False
    print("空字符串 YES")
else:
    print("空字符串 NO")
print("=================================")
if []:  # if [] -> if bool([]) -> if False
    print("[] Yes")
else:
    print("[] No")
print("==================================")
if [False]:  
    # len([False])只要列表的长度不为0,bool([False])的结果就为True
    print("Yes")
print("============================")
if None:  # if bool(None) -> if False
    print("None YES")
else:
    print("None NO")







# if条件判断
if 5 > 3:  # True
    print("Yes")
else:
    print("No")