# 列表 list
alist = [10, 20, "hello", 3.14, True]
print(len(alist))  # 5
# 顺序 -> 序列 -> 索引 -> 切片
print(alist[1])  # 20
print(alist[-1])  # True
print(alist[1:3])  # [20, "hello"]
# 数据类型 type()
print(type(alist))  # <class 'list'>
print(alist + ["test", 10])  # 两个列表首尾拼接
# 修改列表的元素   1查2改
alist[-2] = 100
print(alist)
# 给列表尾部添加元素
alist.append("world")
print(alist)
print("aaaaa" in alist) # False
print("bbbbb" not in alist)  # True
