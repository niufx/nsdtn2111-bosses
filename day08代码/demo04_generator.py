# demo04_generator.py  生成器
def func01():
    a = 10
    return a  # 逻辑执行到return，表示函数的终止
    a = 15
    return a
# print(func01())  # 10
# yield: 用在函数中，表示函数暂停执行
def func02():
    b = 10
    yield b  # 将变量b先返回，然后函数暂停执行
    b = 15
    yield b  # 将变量b先返回，然后函数暂停执行
    b = 30
    yield b  # 将变量b先返回，然后函数暂停执行
jg = func02()  # 调用函数，返回一个生成器对象
# 如果我们不手动调用__next__, 就没有办法获取生成器当中的数据
# 生成器的特点：懒加载(省空间)
print(jg)  # generator object: 生成器
# __next__: 会执行函数内部逻辑，执行到yield时，函数暂停
v1 = jg.__next__()
print(v1)  # 10
v2 = jg.__next__()  # 在上次暂停的位置接着执行
print(v2)  # 15
v3 = jg.__next__()  # 在上次暂停的位置接着执行
print(v3)  # 30
v4 = jg.__next__()  # 报错
print(v4)







