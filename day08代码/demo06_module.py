# demo06_module.py  模块
import sys
print(sys.path)
# import test01_gene
# 在sys.path所指向的目录中一个一个去搜索, 如果搜索成功，则模块导入成功
# 如果在所有路径中都没有搜索到，那么就是导入失败
# ['/root/PycharmProjects/NSD2111/day08',
#  '/root/PycharmProjects/NSD2111',
#  '/root/3/pycharm/pycharm-2020.1.1/plugins/python/helpers/pycharm_display',
#  '/usr/lib64/python36.zip',
#  '/usr/lib64/python3.6',
#  '/usr/lib64/python3.6/lib-dynload',
#  '/usr/lib64/python3.6/site-packages',
#  '/usr/lib/python3.6/site-packages',
#  '/root/3/pycharm/pycharm-2020.1.1/plugins/python/helpers/pycharm_matplotlib_backend']
# 1. 在导入模块之前修改 sys.path
# sys.path.append("/opt/mymodule")  # 临时修改
import test01
test01.func01()
# 2. 通过终端导入环境变量PYTHONPATH  临时修改
# [root@localhost day08]# export PYTHONPATH=/opt/mymodule
# [root@localhost day08]# python3 demo06_module.py
# hello world




# [root@localhost ~]# mkdir /opt/mymodule
# [root@localhost mymodule]# cd /opt/mymodule
# [root@localhost mymodule]# pwd
# /opt/mymodule
# [root@localhost mymodule]# vim test01.py
# [root@localhost mymodule]# cat test01.py
# def func01():
#     print("hello world")
