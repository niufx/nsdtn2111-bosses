# demo03_func.py  函数式编程: 把函数当成参数
# 个位是3和5的数字不要，剩下的添加到新列表
list03 = [25, 63, 27, 75, 70, 83, 27]
# lambda num: True if num % 10 != 3 and num % 10 != 5 else False
# def is_ok(num):
#     return True if num % 10 != 3 and num % 10 != 5 else False
#     # if num % 10 != 3 and num % 10 != 5:
#     #     return True
#     # else:
#     #     return False
jg5 = filter(lambda num: True if num % 10 != 3 and num % 10 != 5 else False, list03)
print(list(jg5))

new_list = []
for item in list03:
    # 对list03列表中每个元素求余数,
    # 如果余数不为3且不为5，那么这个元素添加到新列表
    if item % 10 != 3 and item % 10 != 5:
        new_list.append(item)
print(new_list)



# map(加工函数, 待加工元素列表)
# 会自动将list02当中每一个元素放到cheng_er函数中,赋值给变量num去执行
# 将函数的返回结果添加到新列表
list02 = [1, 2, 3, 4, 5]
# def cheng_er(num):
#     return num * 2
# lambda num: num * 2
jg4 = map(lambda num: num + 10, list02)
print(list(jg4))  # [11, 12, 13, 14, 15]
jg3 = map(lambda num: num * 2, list02)
print(list(jg3))  # [2, 4, 6, 8, 10]

# filter(候选条件函数, 候选者列表)
# 会自动将list01当中每一个元素放到is_oushu函数中,赋值给变量num去执行
# 如果函数执行返回结果为True, 元素留下，添加到新列表
# 如果函数执行返回结果为False, 该元素不会添加到新列表
list01 = [10, 15, 11, 14, 20, 22]
# def is_oushu(num):
#     # 简写方式: if表达式
#     return True if num % 2 == 0 else False
#     # if num % 2 == 0:  # 偶数
#     #     return True
#     # else:
#     #     return False
jg = filter(lambda num: True if num % 2 == 0 else False, list01)
print(list(jg))
# 筛选所有大于10的元素
jg1 = filter(lambda num: True if num > 10 else False, list01)
print(list(jg1))







