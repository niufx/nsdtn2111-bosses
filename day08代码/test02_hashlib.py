# test02_hashlib.py 求一个文件的md5值
import hashlib
def check_md5(fname):  # 计算大文件md5值
    m = hashlib.md5()  # 产生一个可以计算md5值的对象
    frb = open(fname, mode="rb")
    while True:
        data = frb.read(4096)  # 4k  以4K读取效率最高
        if len(data) == 0:  # 文件读取完毕
            break
        m.update(data)  # 将读取到的4K数据进行批量md5值更新
    frb.close()
    return m.hexdigest()  # 文件读取完毕，返回完整文件md5值
if __name__ == '__main__':
    md5 = check_md5("/etc/passwd")
    print(md5)
# py: 3e72425d66ca67348cdc1e57eaaf016a
#     3e72425d66ca67348cdc1e57eaaf016a 
