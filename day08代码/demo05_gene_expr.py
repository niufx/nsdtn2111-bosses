# demo05_gene_expr.py 生成器表达式
import random
# 生成器: 1.懒加载  2.按需分类
# 列表生成式
# 列表: 一次性将列表中所有的数据加载到内存
list02 = [random.randint(1, 10) for i in range(5)]
print(list02)  # [10, 8, 8, 9, 2]
# 生成器表达式
gen01 = (random.randint(1, 10) for i in range(5))
print(type(gen01))  # <class 'generator'>
print(gen01)
# 获取生成器中数据的方式
# 1. gen01.__next__()
# 2. for循环
for i in gen01:
    print("gen:", i)
# >>> sum([i for i in range(1, 100000000)])
# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
#   File "<stdin>", line 1, in <listcomp>
# MemoryError  使用列表计算内存不够
# >>> sum(    (i for i in range(1, 100000000))   )
# 4999999950000000




# list01 = []
# for i in range(5):
#     list01.append(   random.randint(1, 10)    )
# print(list01)

