# demo07_hashlib.py
import hashlib
m = hashlib.md5(b"123456")
print(m.hexdigest())  # 转成16进制
# 分批处理
m1 = hashlib.md5()
m1.update(b"12")
m1.update(b"34")
m1.update(b"56")
print(m1.hexdigest())
