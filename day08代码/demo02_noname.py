# demo02_noname.py
# 匿名函数
def get_sum(x, y):
    return x + y
func01 = lambda x, y: x + y  # 匿名函数
func01(1, 2)  # 不常用, 常用于函数式编程
print(get_sum(10, 20))  # x = 10, y = 20


