# 练习 2：文件生成器
# 需求：通过生成器完成以下功能
# 使用函数实现生成器  yield
# 函数接受一个文件对象作为参数
# 生成器函数每次返回文件的 10 行数据
# 程序 = 数据结构(list) + 算法(open, readline, yield)
# 1.fr=open(),通过fr读取一行一行的数据,每读取一行，给列表中添加一行数据
# 2. 列表长度为10，进行返回yield, 清空列表, 接着读取剩余文件的内容
def gen_file(f):  # f: 文件操作对象
    lines = []  # 每次存储10行数据
    while True:
        line = f.readline()  # 通过f读取一行
        if line == "":  # 表示文件读取完毕
            break
        lines.append(line)  # 如果读到的数据不为空
        if len(lines) == 10:  # 列表长度为10，进行返回yield
            yield lines
            lines.clear()  # 列表清空
    if len(lines) != 0:  # 在文件读取完毕后，再次返回列表数据
        yield lines
if __name__ == '__main__':
    fr = open("/etc/passwd", mode="r")
    jg = gen_file(fr)
    print(type(jg))  # <class 'generator'>
    for item in jg:
        print(item)
    fr.close()

