# demo01_func.py
# 全局变量: 函数外部定义的变量, 函数内部和外部都可以使用
i = 10
def func01():
    print("func in:", i)
func01()  # 10
print("func out:", i)  # 10
# 局部变量: 函数内部定义的变量, 函数内部可用，外部不可用
def func02():
    a = 5
    print("func in a:", a)
func02()  # 5
# print("func out a:", a)  # 函数外部调用局部变量会报错
b = 15
# 就近原则: 函数内部有，使用内部的变量;如果没有要用的变量，在全局变量中寻找
def func03():
    b = 20
    print("func03 in:", b)  # 20,
func03()
print("func03 out:", b)  # 15
# 函数内部修改全局变量的值: global
c = 20
def func04():
    # global 在修改全局变量之前，需要声明函数内部使用的就是全局变量
    global c
    c = c + 20
    print("func04 in:", c)
func04()
# 三种参数: 位置参数 关键字传参 默认参数
# 注意：关键字传参要放到位置传参的后边
def get_info(name, age):
    print("name: %s, age: %s" % (name, age))
get_info("zs", 18)  # 位置参数 name = "zs"  age = 18
get_info(age=18, name="zs")  # 关键字传参 age=18, name="zs"
# get_info(age=18, "zs")  # 报错, 关键字传参要放到位置传参的后边
get_info("zs", age=18)
print("hello", "world", end="\n", sep="-")
