# demo07_write.py
# 1. 打开文件
fw = open("/opt/hello.txt", mode="w")  # w(write): 写文件模式
# 2. 写文件
fw.write("学Python先把这个理念搞懂\n")
fw.write("benben  niupi\n")
# 3. 关闭资源
fw.close()