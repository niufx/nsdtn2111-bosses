# 列表生成式(不是新知识点，是一种简写方式)
list02 = [i**2 for i in range(1, 6)]
print(list02)
# 常规
list01 = []
for i in range(1, 6):
    list01.append(i**2)
print(list01)
