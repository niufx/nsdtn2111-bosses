### 练习：斐波那契数列
# 1. 斐波那契数列就是某一个数，总是前两个数之和
#    0, 1, 1         0+1
#    0, 1, 1, 2      1+1
#    0, 1, 1, 2, 3   1+2
# 2. 使用for循环和range函数编写一个程序，计算有10个数字的斐波那契数列
# 3. 改进程序，要求用户输入一个数字，可以生成用户需要长度的斐波那契数列
num = int(input("一个数:"))
list01 = [0, 1]  # 初始化斐波那契数列
for i in range(num-2):
    list01.append(list01[-1] + list01[-2])
print(list01)
# list01.append(list01[-1]+list01[-2])  # [0, 1, 1]
# list01.append(list01[-1]+list01[-2])  # [0, 1, 1, 2]
# list01.append(list01[-1]+list01[-2])  # [0, 1, 1, 2, 3]