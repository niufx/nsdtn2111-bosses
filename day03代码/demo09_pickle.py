# demo09_pickle.py
import pickle
list01 = [1, "abc", [4, 5]]
fwb = open("/opt/mylist.data", mode="wb")
pickle.dump(list01, fwb)  # 将列表写入文件
fwb.close()
print("======================================")
frb = open("/opt/mylist.data", mode="rb")
data = pickle.load(frb)  # 将列表数据加载出来
print(type(data), data[0])
data.append("1000")
print(data)
frb.close()

