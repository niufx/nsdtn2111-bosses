# demo05_readlines.py
# fr.readlines():
# 将文件中的所有内容按行切分, 将每一行数据添加到列表当中，并返回
# a.txt     fr.readlines()
# aaaa
# bbbb    ->  "aaaa\n", "bbbb\n", "cccc\n", "dddd\n"
# cccc    -> ["aaaa\n", "bbbb\n", "cccc\n", "dddd\n"]
# dddd
# lines = fr.readlines()
# ####lines = ["aaaa\n", "bbbb\n", "cccc\n", "dddd\n"]
# lines[1]: 获取第二行数据
# 1. 打开文件
fr = open("/opt/passwd", mode="r")
# 2. 读取数据
print(fr.readlines())
# 3. 关闭资源
fr.close()

