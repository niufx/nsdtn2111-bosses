# 科学读取大文件 /opt/passwd -> 10G
# 1. 打开文件
fr = open("/opt/passwd", mode="r")
# 2. 读取文件
while True:
    content = fr.readline()
    # 如果content是空字符串的话，表示文件读取完毕
    if content == "":
        break  # 文件读取完毕，退出循环
    print(content, end="")
# 3. 关闭资源
fr.close()
# 1Byte -> 8bit    1bit -> 0/1
# 1Byte -> 11010011
# 1kb/s  1000 * 1/0
# 1KB/s  1000 * 8 * 0/1







