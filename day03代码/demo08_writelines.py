# demo08_writelines.py
list01 = ["hello\n", "world\n", "how\n", "are\n", "you\n"]
fw = open("/opt/b.txt", mode="w")
fw.writelines(list01)
fw.close()
# with: 不是新知识点，是简便写法, 会帮我们关闭资源
list02 = ["aaa\n", "bbb\n", "ccc\n"]
with open("/opt/c.txt", mode="w") as fw1:
    fw1.writelines(list02)