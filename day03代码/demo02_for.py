# range可以和for连用
# 1. 控制for循环的次数: 打印3次helloworld
for i in range(3):
    print("hello world")
# 2. 表示序列的索引标志   11:25上课
# list02 = [1, 2, 3]  每个元素+10
list02 = [1, 2, 3]
for i in range(len(list02)):  # range(3): 0 1 2
    list02[i] += 10  # i = 0, 1, 2
# list02[0] += 10
# list02[1] += 10
# list02[2] += 10
print("list02:", list02)



list01 = [1, 2, -3, 4, -5, 6]
sum_list = 0  # 对列表中所有的数求和
for num01 in list01:
    if num01 <= 0:
        continue  # 跳过本次循环，执行下一次循环
    sum_list += num01  # sum_list = sum_list + num01
print("sum:", sum_list)
print("=========================================")
for num in list01:
    if num <= 0:  # ctrl + /
        break  # 退出循环
    print("num is:", num)