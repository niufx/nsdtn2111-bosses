# demo04_read.py
# 1. 打开文件  cp /etc/passwd /opt/passwd
fr = open("/opt/passwd", mode="r")
# 2. 读取数据(每次读取数据，都会伴随文件指针的移动)
#   1. readline(): 按行读取
print(fr.readline(), end="")  # 读取第一行数据
#   2.1 read(长度): 读取指定长度的数据
print(fr.read(4))
#   2.2 如果read()中的括号什么都不写，读取文件剩余的所有内容
print(fr.read())
# 3. 关闭资源
fr.close()
