# demo01_for.py
# cart 表示购物车
cart = ["巧克力派", "鱿鱼丝", "碎冰冰", "Python从入门到入坟"]
for item in cart:
    print("扫码:", item)
# 求总价
total_price = 0  # 表示总价
cart_price = [10, 20, 30, 99.99]
for money in cart_price:  # for 用于算总价
    total_price += money  # total_price = total_price + money
print("总价:", total_price)
print("===================================")
str01 = "hello world"
for item in str01:
    print(item)

