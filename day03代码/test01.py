#### 练习 5：猜数字程序
# **需求**
# 1. 系统随机生成 100(1~100) 以内的数字
#     import random    random.randint(1, 100)
# 2. 要求用户猜生成的数字是多少   input
# 3. 最多猜 5 次，猜对结束程序
# 4. 如果5次全部猜错，则输出正确结果
import random
number = random.randint(1, 100)  # 最后的答案  number是int类型
counter = 1  # 控制循环的次数
while counter <= 5:  # 当counter 1 2 3 4 5, 用户最多猜5次
    answer = int(input("请猜数(1~100): "))
    if number == answer:  # 表示猜对了, 结束循环: break
        print("你猜对了兄dei~")
        break
    elif answer > number:
        print("猜大了~")
    else:
        print("猜小了~")
    counter += 1  # 猜的次数+1
else:  # 循环正常(非break)结束，执行else逻辑
    print("正确答案是：", number)



