# 模拟 cp 操作
# 创建两个文件操作对象
#   fr(读取/usr/bin/ls)  fw(写/tmp/my_ls)
# 1. 将 /usr/bin/ls rb   "拷贝" 到/tmp/my_ls wb 目录下
# 2. 不要修改原始文件
frb = open("/usr/bin/ls", mode="rb")
fwb = open("/tmp/my_ls", mode="wb")
# 分批读取分批写入   二进制数据没有明显的行概念，所以不能用readline
# frb.read(4096) 4KB=4096B  每次读取4K个数据
while True:
    content = frb.read(4096)  # content: 是字节串数据 b""
    if content == b"":  # 如果content是空字节串,文件读取完毕
        break
    fwb.write(content)
frb.close()
fwb.close()
# 测试
# [root@localhost NSD2111]# chmod 777 /tmp/my_ls
# [root@localhost NSD2111]# /tmp/my_ls ~




