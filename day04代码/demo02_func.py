# 定义一个函数，计算两个数的和
# 函数的返回值return(返回)
# 如果我们没有手动写return，函数内部会自动加上 return None
# return 后的语句都不会在执行, 表示函数执行的结束
def get_sum(num01, num02):
    result = num01 + num02  # result: 结果
    return result
    print("return 后的语句都不会在执行, 表示函数执行的结束")
# a = get_sum(10, 20) 相当于 a = 函数return后的结果
#                           a = result -> a = 10+20
a = get_sum(10, 20)  # num01 = 10, num02 = 20
print(a)  # 拿到结果后可以做后续的逻辑，想打印打印，也可以判断奇数偶数
if a % 2 == 0:
    print("是偶数")
else:
    print("是奇数")















