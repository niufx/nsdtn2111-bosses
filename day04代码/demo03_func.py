# 默认参数
# 1. 如果实际参数给了形参值，那么默认参数不起效果
# 2. 默认参数一定放在形参列表的末尾
def get_sum(num01, num02, num03=10):
    print(num01 + num02 + num03)
get_sum(1, 2, 3)  # num01 = 1 num02 = 2 num03 = 3
get_sum(5, 6)  # num01 = 5 num02 = 6  num03 = 10