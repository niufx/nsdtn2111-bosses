import shutil

# 复制与移动
# 1. shutil.copyfileobj(源文件操作对象, 目标文件操作对象) --只复制内容
fr = open("/etc/passwd", mode="r")
fw = open("/opt/passwd123", mode="w")
shutil.copyfileobj(fr, fw)
fr.close()
fw.close()
# 2. shutil.copyfile(源文件路径, 目标文件路径) --只复制内容
shutil.copyfile("/usr/bin/ls", "/opt/ls321")
# 3. shutil.copy(源文件路径, 目标文件路径)  -- 复制内容和权限
shutil.copy("/usr/bin/ls", "/opt/ls789")
# 4. shutil.move(源文件路径, 目标文件路径)
shutil.move("/opt/ls321", "/opt/ls857")

# 目录操作
# 1. shutil.copytree(源目录路径, 目标目录路径)  -- 递归复制
#    目标目录路径: 确保该路径是不存在的
# shutil.copytree("/etc/security", "/opt/security")
# 2. shutil.rmtree(目录路径)
# shutil.rmtree("/opt/security")
# 权限管理
# 1. shutil.copymode(源文件路径, 目标文件路径) -- 只复制权限
shutil.copymode("/usr/bin/ls", "/opt/passwd123")
# 2. shutil.chown(文件路径, user=None, group=None)
# useradd bobo
shutil.chown("/opt/passwd123", user="bobo", group="bobo")
