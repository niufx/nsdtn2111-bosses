# >>> import os
# >>> os.getcwd()  # pwd
# >>> os.listdir()  # ls
# >>> os.listdir("/etc/security")  # ls /etc/security
# >>> os.mkdir("/tmp/mydir")  # mkdir /tmp/mydir
# >>> os.makedirs("/tmp/mydir1/mydir2")  # mkdir -p /tmp/mydir1/mydir2
# >>> os.chdir("/etc/security")  # cd /etc/security
# >>> os.listdir()  # ls .  查看的路径 /etc/security
# >>> os.remove("/opt/ls789")  # rm -rf /opt/ls789

# demo05_os_path.py
import os
# 1. os.path.abspath(字符串):
#    将字符串和当前所处的路径拼接成一个绝对路径
print(os.path.abspath("abc.txt"))
# /root/PycharmProjects/NSD2111/day04/abc.txt
# 2. os.path.basename(绝对路径): 路径中最后一个斜杠右边的内容
print(os.path.basename("/etc/security/console.apps/config-util"))
# config-util
# 3. os.path.dirname(绝对路径): 路径中最后一个斜杠左边的内容
print(os.path.dirname("/etc/security/console.apps"))
# 4. os.path.join(): 拼接路径
print(os.path.join("/tmp/abc", "test.py"))  # /tmp/abc/test.py
# /tmp/abc/test.py
# 5. os.path.exist(路径): 判断路径存在(True)不存在(False)
print(os.path.exists("/tmp/demo01.py"))  # False
print(os.path.exists("/etc/hosts"))  # True



