# demo01_func.py
# wangwang(wai) bobo(nei)
# 定义函数: 封装功能，不会执行内部逻辑
# 定义函数时的变量something, xidiji是形式参数
def love_xiyiji(something, xidiji):  # 买来洗衣机，没有使用
    print("打水")
    print("放" + xidiji)
    print("洗" + something)
    print("甩干")
# 调用函数：执行函数内部的逻辑  "衣服", "洗衣液"是实际参数
love_xiyiji("衣服", "洗衣液")# something="衣服" xidiji="洗衣液"
love_xiyiji("床单", "五粮液")
love_xiyiji("被罩", "洗衣粉")
# 早上洗衣服
# print("打水")
# print("洗衣服")
# print("甩干")
# # 下午洗衣服
# print("打水")
# print("洗衣服")
# print("甩干")
# # 晚上洗衣服
# print("打水")
# print("洗衣服")
# print("甩干")
