# demo06_subprocess.py
import subprocess
subprocess.run(["ls", "/home"])  # ls /home
subprocess.run(["echo", "$HOME"])  # 无法读取环境变量
# returncode: 最后一条指令执行的结果，成功结果为0, 失败为非0
jg = subprocess.run("echo $HOME", shell=True)  # 可以读取环境变量
print("jg:", jg)
jg1 = subprocess.run("id wangwang", shell=True)
print("jg1:", jg1)
jg2 = subprocess.run("echo $HOME;id wangwang", shell=True)
print("jg2:", jg2)  # returncode=1
jg3 = subprocess.run("id wangwang;echo $HOME", shell=True)
print("jg3:", jg3)  # returncode=0
