### 练习 3：斐波那契数列函数
# 斐波那契数列函数 [0, 1, 1, 2, 3]
# - 将斐波那契数列代码改为函数
# - 数列长度由用户指定
# - 要求把结果用 return 返回
def sc_feibo():
    list01 = [0, 1]
    n = int(input("长度: "))
    for i in range(n - 2):
        list01.append(list01[-1] + list01[-2])
    return list01
result = sc_feibo()  # result = list01
print(result)











