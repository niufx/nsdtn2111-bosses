import sys
def copy(src_name, dest_name):  # src_name: 源路径  dest_name: 目标路径
    frb = open(src_name, mode="rb")
    fwb = open(dest_name, mode="wb")
    while True:
        content = frb.read(4096)
        if content == b"":
            break
        fwb.write(content)
    frb.close()
    fwb.close()
# src_name = "/etc/hosts", dest_name = "/tmp/my_hosts"
copy(sys.argv[1], sys.argv[2])
# 终端
# python3 test02_cp.py /etc/passwd /opt/mypasswd






