# demo04_tuple.py
# 元组(tuple)就是不可变序列,有顺序,使用小括号包裹元素
# 1. 不可变(元组的长短，元素的修改)  2. 存放的数据相对来说安全一些
tuple01 = (100, 20, 30, 55, "zhangsan")
print(tuple01[2])  # 30     17:30 上课
print(tuple01[1:3])  # (20, 30)
print(type(tuple01))  # 查看类型 <class 'tuple'>
tuple02 = ("aaa")
print(type(tuple02))  # 查看类型 <class 'str'>
# 如果元组当中只有单个元素，那么需要在该元素后加逗号
tuple03 = (30, )
print(type(tuple03))  # 查看类型 <class 'tuple'>

