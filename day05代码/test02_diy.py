# 需求
# 编写第一个函数 get_info，接收姓名和年龄，
# 如果年龄不在0到150之间, 产生 ValueError 异常
def get_into():
    name = input("姓名:")
    age = int(input("年龄:"))
    if age < 0 or age > 150:  # 年龄不在1到150之间
        # 1 首先创建一个年龄不在1到150之间的异常
        # raise ValueError("年龄不在0到150之间？？？")
        yichang = ValueError("年龄不在0到150之间？？？")
        # 2 让用户看到错误->抛出异常 raise
        raise yichang
    else:
        print(name, age)
# ValueError: 年龄不在0到150之间？？？
# :右边是报错具体原因
try:
    get_into()  # 可能会出ValueError的异常
except ValueError as e:  # 获取报错的具体原因的方式: as e
    print("错误原因:", e)









