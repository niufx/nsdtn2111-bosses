
li = ["alex", "jerry", "tom", "barry", "wall"]
# 计算列表的 长度 并输出
print(len(li))  # 5
# 请通过步长获取索引为 偶数 的所有值，并打印出获取后的列表
print(li[::2])  # li[:] 不写start和end，默认从头切到尾
# 列表中追加元素 mike，并输出添加后的列表
li.append("mike")
# 请在列表的第 1 个位置插入元素 Tony ，并输出添加后的列表
li.insert(1, "Tony")
# 请修改列表第 2 个位置的元素为 Kelly，并输出修改后的列表
li[2] = "Kelly"
# 请将列表 l2 = [1,”a”,3,4,”heart”] 的每一个元素追加到列表 li 中
l2 = [1, "a", 3, 4, "heart"]
li.extend(l2)
# 请删除列表中的元素 ”barry”，并输出删除后的列表
li.remove("barry")
# 请删除列表中的第 2 个元素，并 输出 删除元素后的列表
li.pop(2)
print(li)
