# try: 把可能执行出错的代码放到try当中, 如果真出问题了，
#      有相对应的处理方案，从而做到不影响try外部后续代码的执行
# except: 用于捕获异常, 找到针对性错误的处理方案
# else: 如果try当中代码没有报错就会执行else中的逻辑
# finally: 翻译最终的, 不管try中的代码有没有报错，都会执行,
#          主要用于对资源的关闭
try:  # try: 试一试
    number01 = int(input("第一个数字: "))  # 可能会出类型转换的问题
    number02 = int(input("第二个数字: "))  # 可能会出类型转换的问题
    jieguo = number01 / number02  # 可能会出除0的问题
    print(jieguo)
except ValueError:  # 应急方案 ValueError: 把非数字的字符串转成的整数报的错误
    print("数字转换异常，请看一下你的输入")
except ZeroDivisionError:  # 除0异常
    print("除数不能为0")
# 当ctrl + c 或者 ctrl + d, 处理方式都是print("Byebye~")
except (KeyboardInterrupt, EOFError):
    print("Byebye~")
except Exception:  # 对于剩余未知的错误，指定一个兜底方案进行统一处理
    print("请您稍后，客服正在努力为您解决~")
# 怎么感觉有了except Exception之后 finally意义不大啊
# 反正就算try里面有错后面的代码都能正常执行
else:
    print("try中的代码没有报错，else正常执行")
finally:
    print("try报不报错都会执行finally中的逻辑")
print("不影响try外部后续代码的执行")
