# demo02_diy-exception.py  python自定义异常
# 我们规定用户注册时输入的密码长度不足8位是有问题的, 应报错
# 定义 `input_password` 函数，提示用户输入密码
def input_password():
    # 1. 提示用户输入密码
    password = input("输入密码:")
    # 2. 判断密码的长度, 如果(len)密码长度>=8, 正常打印
    if len(password) >= 8:
        print(password)
    else:  # 3. 判断密码的长度, 如果密码长度<8, 让代码报错
        # 3.1 首先创建一个密码长度不足8位的异常
        yichang = Exception("密码长度<8, 再多写几位")
        # 3.2 让用户看到错误->抛出异常 raise
        raise yichang
input_password()



