# demo03_list.py
# 修改元素
# >>> list01 = ["zs", "ls", "ww"]
# >>> list01[1] = "zl"  # 修改，将索引为1的元素，修改成zl
# >>> list01
# ['zs', 'zl', 'ww']
# 列表的其他操作
list02 = [6, 1, 2, -5, 0, 10, 1]
# len(列表): 对列表求长度, 就是查看列表中有多少个元素
print(len(list02))  # 6
# reverse(): 对列表进行翻转操作
list02.reverse()
print(list02)  # [1, 10, 0, -5, 2, 1, 6]
# sort(): 升序排列
list02.sort()
print(list02)
# sort(reverse=True): 降序排列
list02.sort(reverse=True)
print(list02)
# count(元素): 统计列表中某个元素出现的次数
print(list02.count(1))  # 2
print("====================================")
boyuwang = ["nfx"]
# 元素添加
boyuwang.append("benben")  # append(元素): 给尾部追加元素
print(boyuwang)  # ['nfx', 'benben']
# insert(索引位置, 元素): 给列表指定位置插入元素
boyuwang.insert(1, "lsj")
print(boyuwang)  # ['nfx', 'lsj', 'benben']
# extend(列表): 将列表中的元素逐一的添加到目标列表list01
# extend与append的区别: append添加的是整体
boyuwang.extend(["bobo", "wangwang"])
print(boyuwang)  # ['nfx', 'lsj', 'benben', 'bobo', 'wangwang']
print("========================================")
# 删除元素
# remove(元素): 删除指定的元素, 没有返回值
boyuwang.remove("benben")
print(boyuwang)
# pop(): 默认删除列表尾部的元素, 将删除的元素进行返回
# pop(索引): 删除指定位置的元素, 将删除的元素进行返回
item = boyuwang.pop()  # 将尾部删除的元素赋值给变量item
print(item)
print(boyuwang)  # ['nfx', 'lsj', 'bobo']
# del 删除的元素: 通用的删除方式, 没有返回值
del boyuwang[2]
print(boyuwang)  # ['nfx', 'lsj']
# clear(): 清空列表
boyuwang.clear()
print(boyuwang)  # []
# 修改元素
# >>> list01 = ["zs", "ls", "ww"]
# >>> list01[1] = "zl"  # 修改，将索引为1的元素，修改成zl
# >>> list01
# ['zs', 'zl', 'ww']

