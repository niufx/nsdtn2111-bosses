# test_import.py
# 保证 my_cp 和 test_import 两个.py文件在同一目录
# 完整文件复制的功能
import my_cp  # 在 test_import 中导入 my_cp.py
# 导入my_cp，就会将my_cp中的代码从头到尾都执行一遍
# 在test_import.py调用my_cp.py中的copy方法
my_cp.copy("/etc/passwd", "/tmp/mypasswd")
