# 字典 dict01 = {"k1": "v1", "k2": "v2", "k3": [11,22,33]}
dict01 = {"k1": "v1", "k2": "v2", "k3": [11,22,33]}
# a. 请循环输出所有的key
for key in dict01.keys():
    print("key:", key)
# b. 请循环输出所有的value
for value in dict01.values():
    print("value:", value)
# c. 请循环输出所有的key和value
for item in dict01.items():
    print("key & value:", item)
# d. 请在字典中添加一个键值对，"k4": "v4"，输出添加后的字典
dict01["k4"] = "v4"
# e. 请在修改字典中 "k1" 对应的值为 "alex"，输出修改后的字典
dict01["k1"] = "alex"
# f. 请在k3对应的值中追加一个元素 44，输出修改后的字典
list01 = dict01["k3"]  # list01: [11, 22, 33]
list01.append(44)
# 合并为1行: dict01["k3"].append(44)
# g. 请在k3对应的值索引为 1 的位置插入个元素 18，输出修改后的字典
list01 = dict01.get("k3")
list01.insert(1, 18)
print(dict01)
# {'k1': 'alex', 'k2': 'v2', 'k3': [11, 18, 22, 33, 44], 'k4': 'v4'}



