# __name__: python解释器帮我们定义好的变量
# print(__name__)  # 直接在a.py中右键运行，__name__: __main__
# 如果通过b.py执行a.py的逻辑, __name__: a
def func01():
    print("hello world")
if __name__ == "__main__":  # "a" == "__main__"
    print("这是A的逻辑不想让别人通过调用模块的方式看到")
