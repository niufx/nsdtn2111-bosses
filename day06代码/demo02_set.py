# demo02_set.py  集合: set
# 集合：就是没有value的字典
# 1.元素不可重复  2.元素不可变类型  3.无序
set01 = {"hello", 123, 4.56, 123, "hello"}
print(set01)  # {123, 'hello', 4.56}, 进行去重操作
# set02 = {[1, 2, 3], 123, "abc"}
# print(set02)  # 失败，因为[1, 2, 3]是可变类型，不能存到集合中
set02 = {1, 2, "abc", "456"}
print(1 in set02)  # True 判断某个元素在不在集合中
print(len(set02))  # 4 求集合元素的个数
for item in set02:
    print(item)
# 添加元素 -> 集合.add(元素), 元素必须是不可变的数据类型
set02.add("10")
# 删除元素 -> 1. 集合.discard(元素)    discard: 丢弃
set02.discard("abc")  # 删除不存在的元素不会报错
print(set02)
# 删除元素 -> 2. 集合.remove(元素)
# set02.remove(100)  # 删除不存在的元素会报错
# 求交集并集差集
s1 = {"zs", "ls", "ww"}
s2 = {"ww", "zl", "wangwang"}
print(s1 & s2)  # 交集 "ww"
print(s1 | s2)  # 并集 {"zs", "ls", "ww", "zl", "wangwang"}
print(s1 - s2)  # 差集 {"zs", "ls"}
print(s2 - s1)  # 差集 {"zl", "wangwang"}
# 对列表元素去重  将列表转换成集合类型  set(列表)
list01 = [11, 11, 2, 2, 3, 3, 4]
print(list(set(list01)))
