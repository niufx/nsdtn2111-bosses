# 码云day04代码 -> 将test02_cp.py代码复制到day06 -> my_cp.py
import sys
def copy(src_name, dest_name):  # src_name: 源路径  dest_name: 目标路径
    frb = open(src_name, mode="rb")
    fwb = open(dest_name, mode="wb")
    while True:
        content = frb.read(4096)
        if content == b"":
            break
        fwb.write(content)
    frb.close()
    fwb.close()
# __name__在本模块调用，该值__main__
# 通过其他模导入, __name__就是模块名
# __name__值的变化，对测试代码进行隔离
if __name__ == "__main__":
    copy(sys.argv[1], sys.argv[2])  # 本模块自己的测试
