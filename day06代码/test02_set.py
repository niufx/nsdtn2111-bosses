# 需求
# 有两个文件：a.log 和 b.log
# 两个文件中有大量重复ip   ----->   对每个文件中的ip进行去重
# 取出只有在 b.log 中存在的行 -----> setb - seta
fr = open("/opt/a.log", mode="r")
line_list = fr.readlines()
seta = set(line_list)  # 对列表元素进行去重
fr.close()
fr1 = open("/opt/b.log", mode="r")
line1_list = fr1.readlines()
setb = set(line1_list)  # 对列表元素进行去重
fr1.close()
print(setb - seta)  # 只访问B服务器的ip地址








# vim /opt/a.log
# 192.168.1.100
# 192.168.1.101
# 192.168.1.100
# 192.168.1.103
# vim /opt/b.log
# 192.168.1.100
# 192.168.1.201
# 192.168.1.201
# 192.168.1.104