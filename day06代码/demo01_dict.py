# demo01_dict.py
# dictionary: 字典
# 列表劣势：查询数据，只能按顺序一个一个对比, 没有办法快速定位
# 特点：
# 1.存放的元素是没有顺序的  2.键不能重复  3.key不能是可变的数据类型
# 列  ......  304    键(key): 值(value)  ->  键值对
# 表  ......  234
# 劣  ......  156
# 创建字典
dict01 = {"列": 304, "表": 234, "劣": 156}
# 查询: 通过键获取值  字典名[键]
print(dict01["表"], dict01["列"])  # 234   304
# print(dict01[0])  # 报错
# KeyError: 0   在dict01字典中，找不到0这字对应的页码
# 字典查询
# 姓名: zs  身高: 180  性别: 男
user_info = {"name": "zs", "height": 180, "sex": "nan"}
print(user_info["name"])  # 获取姓名 zs
# 字典名.get(键, 默认值): 如果key不存在，代码不报错
#   如果键存在，则返回正常的value
#   否则，返回默认值,如果不修改默认值，则返回None
jg = user_info.get("wife", "bobo")
print("jieguo:", jg)
keys = user_info.keys()  # 获取字典所有的键(key)
# keys: 长得像列表，可以遍历
for key in keys:
    print("key:", key)
values = user_info.values()  # 获取字典所有的值(value)
for value in values:
    print("value:", value)
print(user_info.items())  # 获取字典所有的键值对(key-value)
# 添加和修改
print("修改前:", user_info)
# 添加键值对 -> 字典名[添加的键] = 添加的值
user_info["age"] = 18
print(user_info)# {'name': 'zs', 'height': 180, 'sex': 'nan', 'age': 18}
# 修改已存在的键对应的值 -> 字典名[已存在的键] = 修改的值
user_info["height"] = 190
print(user_info["height"])  # 190
# 批量修改 字典名.update(新字典)
# 新字典中的key如果在原字典中已存在则表示更新数据, 不存在则表示新添数据
user_info.update({"tizhong": 160, "age": 20})
print(user_info)
# {'name': 'zs', 'height': 190, 'sex': 'nan', 'age': 20, 'tizhong': 160}
# 字典名.setdefault(键, default=默认值)
user_info.setdefault("age", 30)
print(user_info["age"])  # 20, 如果key在字典存在，则数据不更新
user_info.setdefault("dizhi", "beijing")
print(user_info["dizhi"])  # beijing,如果key在字典不存在，则添加
# 删除数据
# 字典名.pop(键)
user_info.pop("dizhi")
# del 删除的数据
del user_info["age"]
# 字典名.clear(): 清空字典   11:17 上课
user_info.clear()
print(user_info)
# len: 计算字典中有多少键值对
dict01 = {"a": 1, "b": 2}
print(len(dict01))  # 2
# 字典的key必须是不可变类型
# 不可变: int str tuple float
# 可变: list, dict





