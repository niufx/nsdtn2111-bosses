class A:
    def func01(self):
        print("A func01")
    def func04(self):
        print("A func04")
class B:
    def func02(self):
        print("B func02")
    def func04(self):
        print("B func04")
class C(A, B):  # 就近原则
    def func03(self):
        print("C func03")
if __name__ == '__main__':
    c1 = C()
    c1.func04()  # class C(A, B): A func04  class C(B, A): B func04
    c1.func01()  # A func01
    c1.func02()  # B func02
    c1.func03()  # C func03