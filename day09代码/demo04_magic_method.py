# 魔术方法
# __init__: 当创建对象时，会自动调用__init__，执行__init__内部的逻辑
# __str__: 自定义直接打印对象引用展示的结果,返回值是字符串类型
# __call__: 把对象的引用当成函数一样调用,
#           执行的逻辑就是__call__方法我们自己写的逻辑
class Book:
    def __init__(self, title, author):
        self.title = title
        self.author = author
    def __str__(self):
        return "书名: %s, 作者: %s"  % (self.title, self.author)
    def __call__(self):
        print("书名: %s, 价格100元" % self.title)
if __name__ == '__main__':
    b1 = Book("Python", "guishu")
    print(b1)  # 书名: Python, 作者: guishu
    # 将b1当成函数一样调用，需要在b1后加括号, 执行的__call__方法内部封装的逻辑
    b1()

