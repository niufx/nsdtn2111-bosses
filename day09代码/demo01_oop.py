# demo01_oop.py  面向对象
# 方法: 定义在类中的函数
class BearToy:  # class 类名:
    # 初始化方法/构造方法: 创建对象时会自动调用的方法,一般用于对象属性的相关操作
    def __init__(self, color, size):
        self.color = color
        self.size = size
    def speak(self):  # 类中定义的方法，就是行为
        # self:表示当前对象,哪个对象调用speak方法,那么self指代的就是这对象
        # 例如: bear1.speak()  -> self: bear1
        print("我是一只 %s %s 泰迪熊玩具" % (self.color, self.size))
bear1 = BearToy("yellow", "big")  # color = "yellow"  size = "big"
bear1.speak()
bear2 = BearToy("black", "small")  # color = "black"  size = "small"
bear2.speak()