# test01_oop.py
# 练习 3：编写游戏人物
# 需求：
# 创建游戏角色类 Role
# 游戏人物角色拥有名字(name)、武器(weapon)属性
# 游戏人物具有攻击(attack)的方法
#     我是谁, 用什么武器, 攻击谁
# 武器通过武器类(Weapon, wname, strength-攻击力)实现
class Role:
    def __init__(self, name, weapon):  # weapon = "丈八蛇矛"
        self.name = name
        self.weapon = weapon  # self.weapon = "丈八蛇矛"
    def attack(self, target):
        print("我是%s,用%s武器,攻击%s,攻击力%s" %
              (self.name, self.weapon.wname, target
               ,self.weapon.strength))
class Weapon:
    def __init__(self, wname, strength):
        self.wname = wname
        self.strength = strength
if __name__ == '__main__':
    w1 = Weapon("丈八蛇矛", 98)  # wname = "丈八蛇矛"  strength = 98
    r2 = Role("张飞", w1)  # name = "张飞"  weapon = w1
    r2.attack("张苞")
    # r2 = Role("张飞", "丈八蛇矛")
    # r2.attack("张苞")  #  target = "张苞"

