import tarfile
# 文件打包
tar = tarfile.open("/opt/my.tar.gz", "w:gz")  # w: 固定写法
tar.add("/etc/passwd")  # 给tar包添加数据
tar.add("/etc/hosts")  # 给tar包添加数据
tar.close()
# 文件解包
tar1 = tarfile.open("/opt/my.tar.gz")
# 解包的过程, path: 解包数据存放的路径
tar1.extractall(path="/opt")
tar1.close()

