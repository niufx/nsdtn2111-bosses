# randpass.py
# 编写一个能生成 8 位随机密码的程序
# 使用 random 的 choice 函数随机取出字符
# 改进程序，用户可以自己决定生成多少位的密码
import random, string
# all_chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
all_chars = string.ascii_letters + string.digits
def get_mima(n=8):
    mima = ""  # 用于保存最后的8位随机密码
    for i in range(n):  # range(8): 控制循环次数为8次
        temp = random.choice(all_chars)  # 每次返回一个随机字符
        mima += temp  # mima = mima + temp, 字符串拼接
    return mima
if __name__ == '__main__':
    jieguo = get_mima(10)  # 将函数返回值赋值给变量jieguo，便于后续使用
    print(jieguo)
    print(get_mima())  # 没有给n传值，n会使用自己的默认值, 8









