# test03_adduser.py
# 练习 5：创建用户
# 需求
# 编写一个函数，实现创建用户的功能
# 提示用户输入 用户名  input
# 随机生成 8位密码 (导入之前的模块文件)
# 创建用户并设置密码 useradd
# 将用户相关信息 写入指定文件 mode="a"
import subprocess, randpass
def adduser():
    # 1.提示用户输入用户名
    name = input("username:").strip()
    # 2.1判断当前用户存在不存在: id 用户
    jg=subprocess.run("id %s &> /dev/null" % name,shell=True)
    # 2.2 returncode值为0表示指令执行成功, 非0表示失败
    if jg.returncode == 0:  # 0表示用户已存在，不能创建
        print("%s已存在" % name)
        return None  # 函数执行结束
    else:
        # 3.1 id指令报错，表示用户名可用, 创建用户: useradd 用户
        subprocess.run("useradd %s" % name, shell=True)
        # 3.2 给创建的用户设置密码  echo 密码 | passwd --stdin 用户
        passwd = randpass.get_mima()  # 调用已有模块中的功能
        subprocess.run(
            "echo %s | passwd --stdin %s" % (passwd, name),
            shell=True
        )
        # 4. 将用户信息写入文件当中
        info = "name: %s  passwd: %s\n" % (name, passwd)
        f = open("/opt/user.txt", mode="a")  # a: 追加写
        f.write(info)
        f.close()
if __name__ == '__main__':
    adduser()




