# demo01_time.py  时间模块
import time
# 1. time.sleep(秒): 让程序阻塞指定的秒数
def banzhuan():
    print("start....")
    print("banzhuaning....")
    time.sleep(3)  # 模拟板砖的过程3s
    print("end....")
# banzhuan()
# 2. time.time(): 返回的是时间戳, 便于时间维度的计算
# start_time = time.time()  # 开始时间
# jieguo = 0
# for i in range(1, 50000001):
#     jieguo += i
# end_time = time.time()  # 结束时间
# print("time:", end_time - start_time)
# 3. time.localtime(时间戳):
# 结构化时间(九元组),如果没有传参,默认是当前时间的时间戳(time.time())
jg = time.localtime()
print(jg)
print(jg[0], jg[1], jg[2])  # 通过索引获取元素
print(jg[:3])  # 通过切片获取元素
print(jg.tm_hour, jg.tm_min, jg.tm_sec)  # 通过属性获取元素
# 2021-09-30 11:23:34 192.168.193.111 / ...........
# 2021-09-30 12:23:34 192.168.193.111 / ...........
# 2021-09-30 13:23:34 192.168.193.111 / ...........
# 4. time.strftime(时间格式, 结构化时间)
# 结构化时间(九元组) 转换成 指定格式的时间字符串
# 结构化时间没有手动赋值, 默认是当前时间的结构化时间数据(time.localtime())
# %Y: 表示年, %m: 表示月, %d: 表示日, %H: 时 %M: 分 %S: 秒
time_str = time.strftime("%Y-%m-%d %H:%M:%S")  # 2022-03-01 22:02:11
print(time_str)  # 2023-06-17 11:22:33
# 5. time.strptime(时间字符串, 时间格式)
# 将指定格式的时间字符串转换成结构化时间(九元组)
time_tuple=time.strptime("3333-12-25 11:22:33", "%Y-%m-%d %H:%M:%S")
print(time_tuple)  # 结构化时间(九元组)
print(time_tuple.tm_wday+1)  # 5