# test02_keyword.py
# 练习 2：测试字符串是否为合法标识符
# 需求
# 编写用于测试字符串的函数
# 函数用于确定字符串是否为合法标识符,可以返回True,否则返回False
# 1.字符串不能为关键字
# 2.第一个字符必须是 字母或下划线 _
# 3.剩下的字符可以是字母和数字或下划线
import keyword, string
def check_var():
    dc = input("请输入:")
    # 用户输入的字符串是关键字，不能作为变量名使用，返回False
    if dc in keyword.kwlist:
        return False
    # 第一个字符不是 字母或下划线，不能作为变量名使用，返回False
    if dc[0] not in string.ascii_letters + "_":
        return False
    # 剩下的字符不是字母和数字或下划线，不能作为变量名使用，返回False
    for item in dc[1:]:
        if item not in string.digits + string.ascii_letters + "_":
            return False
    return True
if __name__ == '__main__':
    print(check_var())











