# 练习 1：取出指定时间段的日志
# 需求
# 有一日志文件，按时间先后顺序记录日志
# 给定 时间范围(09:00~12:00)，取出该范围内的日志
# 自定义日志文件 /opt/myweb.log
import time
# 9点/12点结构化时间
t9=time.strptime("2030-01-02 09:00:00", "%Y-%m-%d %H:%M:%S")
t12=time.strptime("2030-01-02 12:00:00", "%Y-%m-%d %H:%M:%S")
# 1. 读取文件, 获取每一行数据
fr = open("/opt/myweb.log", mode="r")
for line in fr.readlines():  # fr.readlines(): 列表
    # 2. 将每一行日志表示时间的字符串通过切片截取下来
    # 3. 将表示时间的字符串 time.strptime 将其转换成结构化时间
    t = time.strptime(line[:19], "%Y-%m-%d %H:%M:%S")
    # 4. 将转换后的结构化时间同9点结构化时间和12点的结构化时间去比较
    if t9 <= t <= t12:
        print(line, end="")  # 打印出所需要的日志信息
fr.close()

# [root@localhost xxx]# vim /opt/myweb.log
# 2030-01-02 08:01:43 aaaaaaaaaaaaaaaaa
# 2030-01-02 08:34:23 bbbbbbbbbbbbbbbbbbbb
# 2030-01-02 09:23:12 ccccccccccccccccccccc
# 2030-01-02 10:56:13 ddddddddddddddddddddddddddd
# 2030-01-02 11:38:19 eeeeeeeeeeeeeeee
# 2030-01-02 12:02:28 ffffffffffffffff
