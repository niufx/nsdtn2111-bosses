# demo06.requests.py
import requests
# 给爬虫穿上浏览器的衣服进行伪装
#Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0
headers = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0"
}
url = "http://www.jianshu.com"
resp = requests.get(url, headers=headers)  # 发送请求并接受响应
print(resp.text)
