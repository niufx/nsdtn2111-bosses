# demo02_json.py
import json
user_info = {"name": "zs", "age": 18}
# json.dumps(user_info): dict -> json
json_jg = json.dumps(user_info)
print(json_jg, type(json_jg))  # str
# json.loads(json字符串): json -> dict
py_obj = json.loads(json_jg)
print(type(py_obj))  # dict
print(py_obj["name"], py_obj["age"])

