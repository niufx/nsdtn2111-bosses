# demo05_requests.py
# 天气预报查询
# 搜索 中国天气网 城市代码查询， 查询城市代码
# 城市天气情况接口
# 101100801
# 实况天气获取： http://www.weather.com.cn/data/sk/城市代码.html
import requests
url = "http://www.weather.com.cn/data/sk/101100801.html"
resp = requests.get(url)  # 发送请求并接受响应
print(resp.encoding)  # 查看数据编码格式  ISO-8859-1
resp.encoding = "utf8"  # 修改编码格式为utf8
print(resp.encoding)  # 查看数据编码格式  utf8(可以识别中文)
data = resp.json()  # resp是json字符串, 所以可以直接.json获取
print(type(data))  # <class 'dict'>
print(data["weatherinfo"]["WD"], data["weatherinfo"]["SD"])
#
# a = {  # a["weatherinfo"]["WD"]
#  'weatherinfo':
#      {'city': '运城',
#       'cityid': '101100801',
#       'temp': '20.1', 'WD': '南风',
#       'WS': '小于3级', 'SD': '69%',
#       'AP': '967.3hPa',
#       'njd': '暂无实况',
#       'WSE': '<3',
#       'time': '17:00',
#       'sm': '0.9',
#       'isRadar': '0',
#       'Radar': ''}
# }

