# https://www.baidu.com/s?wd=abc
# wd=abc wd: get请求的参数  abc: 请求的参数值
import requests
# 给爬虫穿浏览器的衣服
headers = {"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0"}
url = "https://www.baidu.com/s?"
dc = {"wd": "abc"}  # 给请求封装参数
resp = requests.get(url, params=dc, headers=headers)
print(resp.text)
