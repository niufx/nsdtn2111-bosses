# 需求
# 包子的价格是 **1.5 元/个**
# 买了 **10 个** 包子
# 计算付款金额
price = 1.5
number = 10
money = price * number
print("金额:", money)
# * 今天老板高兴，总价打 9 折   请重新计算购买金额
money *= 0.9  # money = money * 0.9
print("金额:", money)
print("================================")
tmooc_account = "123@qq.com"
tmooc_password = 123456
print(tmooc_account)
print(tmooc_password)