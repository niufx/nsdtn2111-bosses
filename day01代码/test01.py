#### 练习 3：汇率转换
# **需求：**
# - 用户输入(input)美元，编写程序，将美元转换成人民币
# - 汇率换算：美元 -> 人民币
#   - 1美元 = 6.4435人民币
# - 输出结果(多少美元换算为多少人民币,字符串的拼接)
dollar = input("请输入美元数:")
dollar = float(dollar)  # str -> float
rmb = dollar * 6.4435
# float -> str
print(str(dollar) + "美元换算为" + str(rmb)  + "人民币")