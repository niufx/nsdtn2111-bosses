# 字符串 str   ""   ''   """"""  ''''''
str01 = "hello world"
str02 = "I'm 18 years old"  # I'm 18 years old
str03 = """I'm 18 "years" old"""
# """"""包裹的字符串也叫原样字符串
str04 = """one line
two line     hahahaah   1aaaa  
three line    123"""
# print(str04)
str05 = "python"
# 获取字符串的长度
print(len(str05))  # 6
# 索引顺序
print(str05[2])  # t
print(str05[0])  # p
print(str05[4], str05[-2])  # o
# 字符串的切片  str[start:end]
# start和end表示的是字符串的索引
# 1 切片具有含头去尾的特点：能获取到start索引的元素，但是获取不到end索引的元素
# 2 如果start不写，默认从头去切 end不写，默认切到尾部
print(str05[-3:], str05[4:])  # on3
print(str05[:3])  # pyt
# str[start:end:step]
# 保证切片和步长的方向一致，要么都是从左向右，要么都是从右向左
# step: 表示步长, -3(-：从右向左走, 3: 每3步取一次)
# step默认是1: 从左向右，一步一个脚印
str01 = "python"
print(str01[-2:-4:1])   # ""
print(str01[-2:-5:-1])  # oht
print(str01[-2:-5:-2])  # ot
print(str01[::1])  # python
print(str01[::-1])  # nohtyp
# 注意
# print(str01[100]), 100超过了索引范围，会报错
# print(str01[3:100]), 切片时超范围，代码不会报错
# 推荐写法：print(str01[3:])