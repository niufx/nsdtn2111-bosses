# 通过input接收的数据都是字符串(一句话)类型
# +: 保证+左右两边的数据类型一致
num01 = input("第一个数:")  # 此时 num01是一句话
num02 = input("第二个数:")  # 此时 num02是一句话
# 此时的+是首尾拼接操作，不是数学运算
# jieguo = num01 + num02   100200
num01 = int(num01)  # num01 -> int: 整数
num02 = int(num02)  # num02 -> int
jieguo = num01 + num02  # 进行的数学运算的加法
print("这两个数的和:", jieguo)
print("这两个数的和:" + str(jieguo)) # str(): int -> str