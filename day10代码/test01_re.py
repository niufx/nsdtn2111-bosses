# test01_re.py
# 统计每个 ip/浏览器 访问当前服务器的次数 字典
import re
fr = open("/opt/access.log", mode="r")  # 读取日志
# patt = re.compile("Chrome|Firefox|IE")  # 编译正则 re.compile
patt = re.compile("(\d+\.){3}\d+")  # 编译正则 re.compile
ip_times = {}  # 保存每个ip所出现的次数
for line in fr.readlines():
    jg = patt.search(line)  # line: 192.168.1.10 Chrome
    if jg != None:  # 如果能匹配到数据
        ip = jg.group()  # 获取匹配的字符串, 也就是ip地址
        if ip in ip_times.keys():  # 如果该ip不是第一次访问
            ip_times[ip] = ip_times[ip] + 1  # 该ip访问次数+1
        else:  # 如果该ip是第一次访问, 访问次数设置为1
            ip_times[ip] = 1
fr.close()
print(ip_times)  # 展示结果





# vim /opt/access.log
# 192.168.1.101 Chrome
# 192.168.1.102 Firefox
# 192.168.1.102 IE
# 192.168.1.101 IE
# 192.168.1.103 Chrome
# 192.168.1.104 Firefox

# list: ["192.168.1.101", 1, "192.168.1.102", 1]
# dict01 = {"192.168.1.101": 1, "192.168.1.102": 1}
# dict01["192.168.1.102"]  = dict01["192.168.1.102"] + 1
# dict01["192.168.1.102"] += 1


