# demo04_paramiko.py  远程操作
import paramiko  # 跑代码之前保证 ssh 服务启动
def rcmd(host, user, passwd, mingling):
    ssh = paramiko.SSHClient()  # 创建远程连接的客户端
    # 模拟输入yes的过程
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    # 远程连接的ip地址,username:远程连接用户名 password:远程连接用户的密码
    ssh.connect(host, username=user, password=passwd)
    # 远程执行的指令  jg: 是一个元组类型
    jg = ssh.exec_command(mingling)
    # 字节串转换成字符串: 字节串.decode()
    print(jg[1].read().decode())  # 获取执行成功的结果
    print(jg[2].read().decode())  # 获取执行失败的结果
    ssh.close()  # 关闭资源
if __name__ == '__main__':
    rcmd("127.0.0.1", "root", "123", "id wangwang")
