# 查看当前环境有没有指定的模块: pip3 list|grep -i '模块名'
# 如果pip3的版本是9.0.3
#     执行  python3 -m pip install --upgrade pip
# 对pip3 的版本进行更新
# pip3 install paramiko
# pip3 list|grep -i "paramiko"

# 没有外网的同学:
# cd   linux-soft/3/pypkgs/paramiko_pkgs
# pip3 install *
# pip3 list|grep -i "paramiko"


# 下载并安装指定模块: pip3 install 模块名
#    - 默认外网下载
#    - 修改国内源地址

