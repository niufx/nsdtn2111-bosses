# test02_threading.py
# 练习 1：扫描存活主机
# 需求：
# 通过 ping 测试主机是否可达  ping -c 2 host &> /dev/null
# 如果 ping 不通，不管什么原因都认为主机不可用
import subprocess, threading
def ping(host):
    jg = subprocess.run(
        "ping -c 2 %s &> /dev/null" % host, shell=True)
    if jg.returncode == 0:
        print(host + ": up")
    else:
        print(host + ":down")
if __name__ == '__main__':
    wangzhan = ["www.baidu.com", "www.tmooc.cn", "www.163.com",
        "www.baidu.com", "www.tmooc.cn", "www.163.com",
        "www.baidu.com", "www.tmooc.cn", "www.163.com",]
    print("单线程:")
    for ip in wangzhan:  # 单线程
        ping(ip)
    print("多线程:")
    for ip in wangzhan:  # 多线程
        t = threading.Thread(target=ping, args=(ip, ))
        t.start()






# 通过多线程方式实现并发扫描


