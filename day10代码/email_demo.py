# [root@localhost ~]# useradd user01
# [root@localhost ~]# useradd user02
import smtplib
from email.header import Header
from email.mime.text import MIMEText
# plain: 纯文本
msg = MIMEText("python text", "plain", "utf8")  # 构建邮件正文
# 构建邮件头部
msg["From"] = Header("user01", "utf8")  # 发送者
msg["To"] = Header("user02", "utf8")  # 收件人
msg["Subject"] = Header("py test", "utf8")  # 主题
smtp = smtplib.SMTP("127.0.0.1")  # 声明服务器的地址
# sendmail(发送者, 接收者, 邮件)
smtp.sendmail("user01", "user02", msg.as_bytes())
# 执行代码之前
# mailx -u user02
# no mail
# 执行代码之后
# mailx -u user02
# 有邮件信息









# rpm -q postfix || yum -y install postfix
# yum -y install mailx
# systemctl start postfix
# netstat -utnlp | grep :25
