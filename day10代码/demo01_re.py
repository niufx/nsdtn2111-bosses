# demo01_re.py
import re
# re.match(正则, 目标字符串): 判断目标字符串是不是以正则匹配的内容开头
# 如果匹配不到数据返回的结果为None
jg = re.match("\d{3}", "123aaa456")
print(jg)  # SRE_Match object
print(jg.group())  # 获取匹配到的字符串  123
# re.search(正则, 目标字符串): 在目标字符串中搜索正则匹配的数据,匹配即停止
jg1 = re.search("\d{3}", "12qewqez456qweqw789")
print(jg1.group())  # 获取匹配到的字符串 456
# re.findall(正则, 目标字符串): 在目标字符串中返回正则匹配到的所有数据,返回列表
jg2 = re.findall("\d{3}", "12qewqez456qweqw789")
print(jg2)
# re.finditer(正则, 目标字符串):在目标字符串中返回正则匹配到的所有数据,可迭代对象
jg3 = re.finditer("\d{3}", "12qewqez456qweqw789")
for item in jg3:
    print(item.group())  # 获取匹配到的字符串
# re.split(正则, 目标字符串): 通过正则匹配的内容对字符串进行切割
str01 = "hello-world.tar.gz"
jg4 = re.split("-|\.", str01)  # 通过.或者- 对字符串进行切割
print(jg4)
# re.sub(正则, 替换字符串, 目标字符串)
jg5 = re.sub("\d{3}", "nfx", "Hi 123, Nice to meet you, 456")
print(jg5)  # Hi nfx, Nice to meet you, nfx
# re.compile(字符串): 将字符串编译成正则对象   pattern
patt = re.compile("\d{4}")  # patt: 正则对象
jg6 = patt.search("abc1234qwer")  # 直接使用已编译好的正则进行匹配
print(jg6.group())  # 1234
jg7 = patt.findall("abc1234qwer4567")  # 直接使用已编译好的正则进行匹配
print(jg7)  # ["1234", "4567"]







# \d: 表示匹配单个的数字,0123456789
# \d\d\d: 表示匹配连续出现的三个数字
# \d{3}: 表示匹配连续出现的三个数字
# \d+: 表示匹配一个或连续多个数字  100001234567111

# 192.168.1.100 chrome /index 2022-03-07 22:22:22
# 192.168.1.101 firefox /index 2022-03-07 22:23:22
# 192.168.1.102 firefox /index 2022-03-07 22:24:22
# 192.168.1.103 IE /index 2022-03-07 22:25:22
